# Jira Custom Field Tutorial

This tutorial shows you how to create a new custom field type for JIRA using the app development platform. You'll create a custom field that can only be edited as an admin user. 

This tutorial covers the following topics:

* An overview of custom fields and the files that comprise the app
* Extending the custom field type class, GenericTextCFT, for your app
* Using the customfield-type plugin module type for JIRA

You can view the full tutorial here: Creating a Custom Field Type.

## Running locally

To run this app locally, make sure that you have the Atlassian Plugin SDK installed, and then run:
atlas-mvn jira:run